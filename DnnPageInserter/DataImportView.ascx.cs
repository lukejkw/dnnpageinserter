﻿using DotNetNuke.Entities.Modules;
using DotNetNuke.Services.Exceptions;
using DotNetNuke.UI.Skins;
using DotNetNuke.UI.Skins.Controls;
using DnnPageInserter.Components;
using DnnPageInserter.DataServices;
using DnnPageInserter.Models;
using System;
using System.Collections.Generic;
using System.IO;

namespace DnnPageInserter
{
    public partial class DataImportView : PortalModuleBase
    {
        #region Event Handlers

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            cmdInsertDrupalData.Click += ProcessDrupalData;
        }

        private void ProcessDrupalData(object sender, EventArgs e)
        {
            try
            {
                if (!ValidateForm())
                    return;

                // Save file
                string ext = Path.GetExtension(ctlFileUpload.FileName);
                string filePath = $@"{PortalSettings.HomeDirectoryMapPath}\Dnn_Page_Inserter\pages{ext}";
                SaveFile(filePath);

                // Get Drupal page list
                List<ImportPage> pages = PageDataService.GetPagesFromJSONFile(filePath);
                if (pages == null)
                {
                    Skin.AddModuleMessage(this, "No pages found in file. Please check format", ModuleMessage.ModuleMessageType.RedError);
                    return;
                }

                // Create pages
                string rootTab = txtRootTab.Text.Trim();
                List<ImportPage> errored = DNNContentCreator.CreateDnnContentPagesFromDrupalPageData(pages, PortalId, rootTab);

                ShowErrors(pages.Count, errored);
            }
            catch (Exception ex)
            {
                Exceptions.ProcessModuleLoadException(this, ex);
            }
        }

        #endregion Event Handlers

        #region Methods

        private bool ValidateForm()
        {
            // Validate params
            if (!ctlFileUpload.HasFile)
            {
                Skin.AddModuleMessage(this, "Must select a file", ModuleMessage.ModuleMessageType.RedError);
                return false;
            }

            string fileExt = Path.GetExtension(ctlFileUpload.FileName);
            switch (fileExt.ToUpper())
            {
                case ".TXT":
                case ".JSON":
                    Skin.AddModuleMessage(this, "Must select a .txt or .json file", ModuleMessage.ModuleMessageType.RedError);
                    return false;
            }

            string rootTab = txtRootTab.Text.Trim();
            if (String.IsNullOrEmpty(rootTab))
            {
                Skin.AddModuleMessage(this, "Must specifiy tab name", ModuleMessage.ModuleMessageType.RedError);
                return false;
            }
            return true;
        }

        private void SaveFile(string filePath)
        {
            if (File.Exists(filePath))
                File.Delete(filePath);

            string dirPath = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(dirPath))
                Directory.CreateDirectory(dirPath);

            ctlFileUpload.SaveAs(filePath);
        }

        private void ShowErrors(int totalCount, List<ImportPage> errored)
        {
            // Show errored pages
            if (errored.Count > 0)
            {
                pnlErrored.Visible = true;
                ctlErrored.Text = "<ol>";
                foreach (var page in errored)
                    ctlErrored.Text += $"<li>{page.title}<li>";
                ctlErrored.Text += "<ol>";
            }
            else
                pnlErrored.Visible = true;

            // Display message to user
            if (errored.Count < totalCount)
                Skin.AddModuleMessage(this, "Some/all Pages successfully added. Check the bottom of the module for errored pages (if any)", ModuleMessage.ModuleMessageType.GreenSuccess);
            else
                Skin.AddModuleMessage(this, "No pages were successfully added", ModuleMessage.ModuleMessageType.GreenSuccess);
        }

        #endregion Methods
    }
}