﻿using DnnPageInserter.Models;
using DotNetNuke.Common.Utilities;
using DotNetNuke.Entities.Modules;
using DotNetNuke.Entities.Modules.Definitions;
using DotNetNuke.Entities.Portals;
using DotNetNuke.Entities.Tabs;
using DotNetNuke.Modules.Html;
using DotNetNuke.Security.Permissions;
using System;
using System.Collections.Generic;

namespace DnnPageInserter.Components
{
    public class DNNContentCreator
    {
        /// <summary>
        /// Code inspired by https://johnnblade.wordpress.com/2011/03/14/programmatically-add-page-and-html-module-with-content-dotnetnuke-dnn/
        /// </summary>
        /// <param name="drupalPages">A list of DrupalPage</param>
        /// <param name="portalId">PortalId of the site to create pages in</param>
        /// <param name="rootTabName">The Tab name that the tabs should be nested under</param>
        public static List<ImportPage> CreateDnnContentPagesFromDrupalPageData(List<ImportPage> drupalPages, int portalId, string rootTabName)
        {
            var errored = new List<ImportPage>();
            // Init portalsettings and tab controller
            PortalSettings portalSettings = new PortalSettings(portalId);
            string defaultPortalSkin = portalSettings.DefaultPortalSkin;
            string defaultPortalContainer = portalSettings.DefaultPortalContainer;
            TabController tabController = new TabController();

            // Get parent tab
            TabInfo parentTab = tabController.GetTabByName(rootTabName, portalId);
            if (parentTab != null)
            {
                foreach (var page in drupalPages)
                {
                    try
                    {
                        // Dont add pages if any of their data is null
                        if (page.content == null || page.title == null)
                        {
                            // Add to errored
                            errored.Add(page);
                            continue;
                        }

                        // Try get tab
                        TabInfo oldTab = tabController.GetTabByName(page.title, portalId, parentTab.TabID);
                        if (oldTab != null)
                        {
                            // Ignore tab if already created
                            errored.Add(page);

                            // Use the below if you want to rather delete the tab
                            if (oldTab.Modules != null)
                            {
                                foreach (ModuleInfo mod in oldTab.Modules)
                                {
                                    ModuleController moduleController = new ModuleController();
                                    moduleController.DeleteModule(mod.ModuleID);
                                    moduleController.DeleteModuleSettings(mod.ModuleID);
                                }
                            }

                            tabController.DeleteTab(oldTab.TabID, portalId);
                            tabController.DeleteTabSettings(oldTab.TabID);
                            DataCache.ClearModuleCache(oldTab.TabID);
                        }

                        // Init new tab to create
                        TabInfo tab = new TabInfo();
                        tab.PortalID = portalId;
                        tab.TabName = page.title;
                        tab.Title = page.title;
                        tab.Description = page.title;
                        tab.KeyWords = page.title;
                        tab.IsVisible = true;
                        tab.DisableLink = false;
                        tab.ParentId = parentTab.TabID;
                        tab.IsDeleted = false;
                        tab.Url = string.Empty;
                        tab.SkinSrc = defaultPortalSkin;
                        tab.ContainerSrc = defaultPortalContainer;
                        tab.IsSuperTab = false;

                        // Add permission to the tab so that all users can view it
                        foreach (PermissionInfo permissionInfo in PermissionController.GetPermissionsByTab())
                        {
                            if (permissionInfo.PermissionKey == "VIEW")
                            {
                                TabPermissionInfo tpi = new TabPermissionInfo();
                                tpi.PermissionID = permissionInfo.PermissionID;
                                tpi.PermissionKey = permissionInfo.PermissionKey;
                                tpi.PermissionName = permissionInfo.PermissionName;
                                tpi.AllowAccess = true;
                                tpi.RoleID = -1; // ID of all users
                                tab.TabPermissions.Add(tpi);
                            }
                        }

                        // Add tab to DNN and clear cache
                        int tabId = tabController.AddTab(tab, true);
                        DataCache.ClearModuleCache(tab.TabID);

                        // Get HTML module from desktop modules
                        DesktopModuleInfo desktopModuleInfo = null;
                        // Add a module to the newly created page/tab
                        foreach (KeyValuePair<int, DesktopModuleInfo> kvp in DesktopModuleController.GetDesktopModules(portalId))
                        {
                            DesktopModuleInfo mod = kvp.Value;
                            if (mod != null)
                                if (mod.FriendlyName.IndexOf("HTML") > -1 || mod.ModuleName.IndexOf("HTML") > -1)
                                {
                                    desktopModuleInfo = mod;
                                    break;
                                }
                        }

                        // Check if we found HTML module
                        if (desktopModuleInfo != null)
                        {
                            // Iterate over HTML module defs (should only really iterate once)
                            foreach (ModuleDefinitionInfo moduleDefinitionInfo in ModuleDefinitionController.GetModuleDefinitionsByDesktopModuleID(desktopModuleInfo.DesktopModuleID).Values)
                            {
                                // Create module instance
                                ModuleInfo moduleInfo = new ModuleInfo();
                                moduleInfo.PortalID = portalId;
                                moduleInfo.TabID = tabId;
                                moduleInfo.ModuleOrder = 1;
                                moduleInfo.ModuleTitle = page.title;
                                moduleInfo.PaneName = "ContentPane";
                                moduleInfo.ModuleDefID = moduleDefinitionInfo.ModuleDefID;
                                moduleInfo.CacheTime = moduleDefinitionInfo.DefaultCacheTime;
                                moduleInfo.InheritViewPermissions = true;
                                moduleInfo.AllTabs = false;
                                moduleInfo.Alignment = string.Empty;

                                // Add to tab
                                ModuleController moduleController = new ModuleController();
                                int moduleId = moduleController.AddModule(moduleInfo);

                                // Add the content to the module
                                HtmlTextController htmlTextController = new HtmlTextController();
                                WorkflowStateController workflowStateController = new WorkflowStateController();

                                int workflowId = htmlTextController.GetWorkflow(moduleId, tabId, portalId).Value;

                                // Het top workflow item or create it
                                HtmlTextInfo htmlContent = htmlTextController.GetTopHtmlText(moduleId, false, workflowId);
                                if (htmlContent == null)
                                {
                                    htmlContent = new HtmlTextInfo();
                                    htmlContent.ItemID = -1;
                                    htmlContent.StateID = workflowStateController.GetFirstWorkflowStateID(workflowId);
                                    htmlContent.WorkflowID = workflowId;
                                    htmlContent.ModuleID = moduleId;
                                    htmlContent.IsPublished = true;
                                    htmlContent.Approved = true;
                                    htmlContent.IsActive = true;
                                }
                                htmlContent.Content = page.content.Trim().Replace("\"/files", "\"/portals/0/files");

                                int draftStateId = workflowStateController.GetFirstWorkflowStateID(workflowId);
                                int nextWorkflowStateId = workflowStateController.GetNextWorkflowStateID(workflowId, htmlContent.StateID);
                                int publishedStateId = workflowStateController.GetLastWorkflowStateID(workflowId);

                                // Update the html
                                htmlTextController.UpdateHtmlText(htmlContent, htmlTextController.GetMaximumVersionHistory(portalId));
                            }
                        }
                    }
                    catch (Exception)
                    {
                        // Problem adding page
                        errored.Add(page);
                    }
                }
            }
            // Return null pages
            return errored;
        }
    }
}