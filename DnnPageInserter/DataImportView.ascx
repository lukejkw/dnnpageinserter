﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="DataImportView.ascx.cs" Inherits="DnnPageInserter.DataImportView" %>
<div class="dnnForm">
    <p>
        This module creates DNN tabs from Drupal page data.
    </p>
    <p>
        Please ensure that the website database has been backedup before running this task.
    </p>
    <p>
        An example of the way the json needs to be:
    </p>
    <p>
        [{"nid":"1","title":"Placeholder Page","content":"\u003Cp\u003EPlaceholder Page\u003C\/p\u003E","created":"2013-10-09 11:47:49","changed":"2013-10-30 12:14:38"},{"nid":"2","title":"Placeholder Drop-down","content":"\u003Cp\u003EPlaceholder Drop-down\u003C\/p\u003E","created":"2013-10-09 13:56:32","changed":"2013-10-23 10:12:55"},{"nid":"3","title":"Testing Promoted Content","content":"\u003Cp\u003ETesting Promoted Content body\u003C\/p\u003E","created":"2013-10-22 15:38:49","changed":"2013-10-30 12:08:54"}]
    </p>

    <div class="dnnFormItem">
        <label>Please Select your JSON file</label>
        <asp:FileUpload ID="ctlFileUpload" ToolTip="Select a json file" runat="server"/>    
    </div>
    <div class="dnnFormItem">
        <label>Please enter the root tab name</label>
        <asp:TextBox 
            ID="txtRootTab" runat="server"
            placeholder="HOME"  />
        <asp:RequiredFieldValidator 
            ID="RequiredFieldValidator1" runat="server"
            Text="Must enter root tab" ErrorMessage="Must enter root tab" 
            ControlToValidate="txtRootTab" 
            ForeColor="Red" 
            SetFocusOnError="true" />
    </div>
    <ul class="dnnActions">
        <li>
            <asp:LinkButton ID="cmdInsertDrupalData" runat="server"
                Text="Insert drupal pages" CssClass="dnnPrimaryAction" />
        </li>
    </ul>

    <asp:Panel ID="pnlErrored" Visible="false" runat="server">
        <h3>The following pages were not added</h3>
        <asp:Literal ID="ctlErrored" runat="server" />
    </asp:Panel>
</div>