﻿using DnnPageInserter.Models;
using System;
using System.IO;
using System.Collections.Generic;
using System.Web.Script.Serialization;

namespace DnnPageInserter.DataServices
{
    public class PageDataService
    {
        public static List<ImportPage> GetPagesFromJSONFile(string jsonFilePath)
        {
            try
            {
                // Read JSON from file
                string json = File.ReadAllText(jsonFilePath);

                // Hydrated Drupal page list
                var pages = new JavaScriptSerializer().Deserialize<List<ImportPage>>(json);

                // Return
                return pages;
            }
            catch (Exception)
            {
                // Error accessing the file
                return null;
            }
        }
    }
}
