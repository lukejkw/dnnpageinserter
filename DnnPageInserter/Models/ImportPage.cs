﻿using System;

namespace DnnPageInserter.Models
{
    public class ImportPage
    {
        public int nid { get; set; }
        public string title { get; set; }
        public string content { get; set; }
        public DateTime created { get; set; }
        public DateTime changed { get; set; }
    }
}